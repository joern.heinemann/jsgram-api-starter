import {asset, DIContainerInterface, item} from "./DIContainerInterface";

export class DIContainer implements DIContainerInterface
{
	private container: Map<string,item> = new Map();

	/**
	 * @inheritDoc
	 */
	public set(key: string, cb:asset)
	{
		this.container.set(key,{asset:cb, done:false});

		return this;
	}

	/**
	 * @inheritDoc
	 */
	public setStatic(key: string, value: any)
	{
		this.container.set(key,{asset:value, done:true})
	}

	/**
	 * @inheritDoc
	 */
	public has(key: string): boolean
	{
		return this.container.has(key);
	}

	/**
	 * @inheritDoc
	 */
	public get(key: string): any
	{
		let item = this.container.get(key);

		if(!item) {
			throw "Dependency for "+key+" not found";
		}

		if(item.done) {
			return item.asset;
		}

		let asset: any;

		if(typeof item.asset === "function") {
			//di noch nicht resolve
			asset = DIContainer.resolveCb(item.asset);
		} else {
			throw "Asset needs to be a function. Try setStatic instead of set!";
		}

		item.asset = asset;
		item.done = true;

		return asset;
	}

	private static resolveCb(cb: () => any)
	{
		return cb();
	}
}