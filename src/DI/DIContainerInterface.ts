export type asset =  () => any;
export type item = {asset: asset | any, done: boolean};

export interface DIContainerInterface
{
	/**
	 * Set a new value into the container
	 *
	 * The value will be created when it is called for the first time
	 *
	 * @param {string} key
	 * @param {asset} cb
	 * @returns {this}
	 */
	set(key: string, cb:asset);

	/**
	 * Sets a new value into the container
	 *
	 * The value will just returned when it is called
	 * there is no extra steps like @see set()
	 *
	 * @param {string} key
	 * @param value
	 */
	setStatic(key: string, value: any);

	/**
	 * Checks if an item exits in the container
	 *
	 * @param {string} key
	 * @returns {boolean}
	 */
	has(key: string): boolean;

	/**
	 * Returns the value to the given key
	 *
	 * the value will be created first if necessary
	 *
	 * @param {string} key
	 * @returns {}
	 */
	get(key: string): any;
}