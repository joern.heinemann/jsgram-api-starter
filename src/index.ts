import {DIContainer} from "./DI/DIContainer";
import {DIContainerInterface} from "./DI/DIContainerInterface";
import {parse} from "querystring";
import {NextFunction, Response, ServerRequest, SimpleBody} from "jsgram";

let diContainer: DIContainerInterface;

/**
 * Singleton for the DI Container
 *
 * @returns {DIContainerInterface}
 */
export function getDi() {
	if(!diContainer) {
		diContainer = new DIContainer();
	}

	return diContainer;
}

export {
	DIContainerInterface,
	DIContainer
};

//Caches
export {CacheItem,CacheInterface} from "./Cache/CacheInterface";

export {ArrayCache} from "./Cache/ArrayCache"

export {FilesystemCache} from "./Cache/FilesystemCache"

//Middleware
export {JwtAuth} from "./Middleware/JwtAuth"

export function parseGet(req: ServerRequest, res: Response, next: NextFunction) {
	// @ts-ignore
	const query: string = req.urlParts.query;

	req.setAttribute("queryParts",parse(query));

	return next();
}

const sb = new SimpleBody();

export function jsonBody(req: ServerRequest, res: Response, next: NextFunction) {
	sb.read(req)
		.then((body) => {
			req.body = JSON.parse(body);
			return next();
		})
		.catch((err) => {
			return next(err, 413);
		});
}

export function checkLogin(req: ServerRequest, res: Response, next: NextFunction) {
	const user: object | boolean = req.getAttribute("user");

	if(user === false) {
		return next({err: "not logged in"},401);
	}

	return next();
}

export function checkNaN(req: ServerRequest, res: Response, next: NextFunction) {
	const id: number = req.param.get("id");

	if(id === undefined || id === null || isNaN(id)) {
		return next({err: "id is not valid"});
	}

	return next();
}