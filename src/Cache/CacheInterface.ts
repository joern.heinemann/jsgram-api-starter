/**
 * @author Jörn Heinemann <joernheinemann@gmx.de>
 * @since 2020/10/08
 */

export type CacheItem = {
	assert: any;
	ttl: number;
};

export interface CacheInterface
{
	get(key: string): Promise<any>;

	set(key: string, value: any, ttl?: number): Promise<void>;

	delete(key: string | string[]): Promise<boolean>;

	has(key: string): Promise<boolean>;
}