/**
 * @author Jörn Heinemann <joernheinemann@gmx.de>
 * @since 2020/10/11
 */
import {CacheInterface, CacheItem} from "./CacheInterface";

const fs = require("fs").promises;

export class FilesystemCache implements CacheInterface
{

	constructor(private path: string) {}

	public async delete(key: string | string[]): Promise<boolean>
	{
		if(!Array.isArray(key)) {
			const path = this.path+key;

			try {
				await fs.unlink(path);
			} catch (e) {
				return false;
			}

			return true;
		}

		const p = [];

		for (let del of key) {
			p.push(fs.unlink(this.path+del));
		}

		try {
			await Promise.all(p);
		} catch (e) {
			return false;
		}

		return true;
	}

	public async get(key: string): Promise<any>
	{
		const item = await this.getItem(key);

		if(!item) {
			return false;
		}

		return item.assert;
	}

	public async has(key: string): Promise<boolean>
	{
		return ! !await this.getItem(key);
	}

	public async set(key: string, value: any, ttl?: number): Promise<void>
	{
		if(ttl > 0) {
			ttl = Date.now() + ttl * 1000 // in sec
		}

		const item: CacheItem = {
			assert: value,
			ttl: ttl
		};

		const path = this.path+key;

		return fs.writeFile(path, JSON.stringify(item));
	}

	protected async getItem(key: string)
	{
		const path = this.path+key;

		let file, item: CacheItem;

		try {
			await fs.stat(path);

			file = await fs.readFile(path,{encoding:"utf8",flag:"r"});

			item = JSON.parse(file);
		} catch (e) {
			return false;
		}

		if(item.ttl !== 0 && item.ttl < Date.now()) {
			//time to live exp
			await this.delete(key);

			return false;
		}

		return item;
	}
}