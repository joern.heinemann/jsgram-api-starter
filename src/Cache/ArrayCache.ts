/**
 * @author Jörn Heinemann <joernheinemann@gmx.de>
 * @since 2020/10/08
 */
import {CacheInterface, CacheItem} from "./CacheInterface";

export class ArrayCache implements CacheInterface
{
	private cache: Map<string,CacheItem> = new Map();

	public async delete(key: string | string[]): Promise<boolean>
	{
		if(!Array.isArray(key)) {
			return this.cache.delete(key);
		}

		for (let del of key) {
			this.cache.delete(del);
		}
	}

	public async get(key: string): Promise<any>
	{
		const item = this.getItem(key);

		if(!item) {
			return false;
		}

		return item.assert;
	}

	public async set(key: string, value: any, ttl: number = 0)
	{
		if(ttl > 0) {
			ttl = Date.now() + ttl * 1000 // in sec
		}

		const item: CacheItem = {
			assert: value,
			ttl: ttl
		};

		this.cache.set(key,item);
	}

	public async has(key: string): Promise<boolean>
	{
		return ! !this.getItem(key);
	}

	protected getItem(key: string)
	{
		if(!this.cache.has(key)) {
			return false;
		}

		const item: CacheItem = this.cache.get(key);

		if(item.ttl !== 0 && item.ttl < Date.now()) {
			//time to live exp
			this.cache.delete(key);

			return false;
		}

		return item;
	}
}