/**
 * @author Jörn Heinemann <joernheinemann@gmx.de>
 * @since 2020/10/08
 */

import {NextFunction, Response, ServerRequest} from "jsgram";
import {verify, VerifyErrors} from "jsonwebtoken";

export class JwtAuth 
{
	constructor(private key: string) {}

	public process(req: ServerRequest, res: Response, next: NextFunction)
	{
		const token = this.getFromReq(req);

		if(token === false) {
			//no token to check
			req.setAttribute("user",false);
			return next();
		}

		verify(token,this.key, (err: VerifyErrors | null, decoded: object | undefined | any) => {
			if(err) {
				return next({err: "token can't be verified"},401);
			}

			req.setAttribute("user",decoded.data);
			return next();
		})
	}

	protected getFromReq(req: ServerRequest)
	{
		const header = req.headers.authorization;

		if(header) {
			//jwt from header
			return header.replace("Bearer ","");
		}

		//from cookie

		//todo cookie

		return false;
	}
}